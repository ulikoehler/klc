+++
title = "G1.3 Libraries are organized by functionality"
+++

Rather than grouping components (e.g. symbols, footprints) by their manufacturer, KiCad libraries are organized by component functionality. This grouping strategy has a number of key benefits:

* Similar components are grouped together, allowing alternative parts to be easily substituted
* If possible, symbols should be _derived_ from existing symbols to reduce library size
* Generic parts which are produced by multiple manufacturers are supported

Library organization should follow the general form as described below, with each element separated by the underscore (`_`) character:

. Library function
. Library sub-function
. Tertiary qualifier
. Manufacturer name
. Component series name
. Extra library descriptors

_Note: Some of the elements listed above may be omitted if not required._

For specific examples, refer to the guidelines for organizing link:/symbol/s1/s1.1[symbol libraries] and link:/footprint/f1/f1.1[footprint libraries].
